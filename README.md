# Проект визуализации графа

Данный проект направлен на создание инструмента визуализации графа с реализацией различных алгоритмов работы с графами. Инструмент позволит пользователям интерактивно создавать и визуализировать графы, а также применять алгоритмы вроде поиска в глубину (DFS), поиска в ширину (BFS) и алгоритма Дейкстры.

## Функциональность
- Графический интерфейс для интерактивного создания и визуализации графов
- Возможность добавления вершин и рёбер в граф
- Реализации графовых алгоритмов: DFS, BFS, алгоритм Дейкстры

## Архитектура
Проект будет содержать следующие классы:
- Vertex: Класс, представляющий вершину в графе с методами для добавления, удаления, перемещения, и применения алгоритмов DFS, BFS, и Дейкстры.
- Edge: Класс, представляющий ребро в графе с методами для добавления и удаления.

